package com.iwant.userservice.api.controller;

import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.iwant.userservice.data.models.AreaModel;
import com.iwant.userservice.services.common.CommonServices;

@RestController
public class AreaController {

	private static final Logger logger = LoggerFactory.getLogger(AreaController.class);
	
	@Autowired
	CommonServices commonServices;
	
	@PostMapping("/api/common/area")
	public AreaModel createArea(@RequestBody AreaModel area) {
		 logger.trace("AreaController.createArea()");
		
		return commonServices.createNewArea(area);
	}

}
