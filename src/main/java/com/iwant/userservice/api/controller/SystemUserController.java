package com.iwant.userservice.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.iwant.userservice.data.models.security.SystemUser;
import com.iwant.userservice.services.interfaces.SystemUserService;
import com.iwant.userservice.wrapper.UserRequest;

@RestController
public class SystemUserController {

	@Autowired
	SystemUserService userService;
	
	@PostMapping("/api/users/create")
	public SystemUser create(@RequestBody SystemUser req) {
		return userService.create(req);
	}
}
