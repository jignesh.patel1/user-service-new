package com.iwant.userservice.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.iwant.userservice.data.models.CountryModel;
import com.iwant.userservice.services.interfaces.CountryService;

@RestController
public class CountryController {
	
	@Autowired
	CountryService service;
	
	@PostMapping("/api/common/country")
	public CountryModel create(@RequestBody CountryModel input) {
		return service.createNewCountry(input);
	}
	
	@GetMapping("api/common/country")
	public List<CountryModel> getAll( @RequestParam("domain") String domain){
		return service.findAllByDomain(domain);
	}

}
