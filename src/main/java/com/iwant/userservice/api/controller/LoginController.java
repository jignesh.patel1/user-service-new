package com.iwant.userservice.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.iwant.userservice.data.models.security.OtpModel;
import com.iwant.userservice.services.interfaces.OtpService;
import com.iwant.userservice.wrapper.MessageRecipient;
import com.iwant.userservice.wrapper.OtpResponse;
import com.iwant.userservice.wrapper.OtpValidation;


@RestController
public class LoginController {

	@Autowired
	OtpService otpService;

	
	@PostMapping("/user/login/otp")
	public OtpResponse sendOtp(@RequestBody MessageRecipient user) {
		return otpService.sendOtp(user.getDomain(), OtpModel.OTP_LOGIN, user.getMcc(), user.getMobile(), user.getEmail(), null);
	}
	@PostMapping("/user/login/validateotp")
	public OtpValidation validateOtp(@RequestBody OtpValidation req) {
			
		return otpService.validateOtp(req.getTransactionCode(), req.getOtpValue(), req.getUsername(), req.getDomain());
	}
}
