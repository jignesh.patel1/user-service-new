package com.iwant.userservice.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.iwant.userservice.data.models.StateModel;
import com.iwant.userservice.services.interfaces.StateService;

@RestController
public class StateController {

	@Autowired
	StateService stateManager;
	@PostMapping("/api/common/states")
	public StateModel create(@RequestBody StateModel input) {
		return stateManager.create(input);
	}
	
	@PutMapping("/api/common/states")
	public StateModel update(@RequestBody StateModel input) {
		return stateManager.update(input);
	}
	
	@DeleteMapping("/api/common/states/{id}")
	public StateModel delete(@PathVariable String id) {
		return stateManager.findAndRemove(id);
	}
	
	@GetMapping("/api/common/states")
	public List<StateModel> getAll(@RequestParam String domain){
		return stateManager.findAll(domain);
	}
	
	@GetMapping("/api/common/states/filterbycountry/{id}")
	@PreAuthorize("hasAuthority('SYSTEM_ADMIN') or hasAuthority('ROLE_VIEW_STATE')")
	public List<StateModel> getByCountry(@PathVariable String id){
		return stateManager.findAllByCountry(id);
	}
	
	@GetMapping("/api/common/states/{id}")
	public StateModel get(@PathVariable String id){
		return stateManager.findOneById(id);
	}
	
	
}
