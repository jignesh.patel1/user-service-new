package com.iwant.userservice.wrapper;

import java.time.ZonedDateTime;
import java.util.Date;

public class OtpResponse {
	private String transactionId;
	private String transactionCode;
	private int validityInSeconds;
	private String timeZoneId;
	private ZonedDateTime expiry;
	
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getTransactionCode() {
		return transactionCode;
	}
	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}
	public int getValidityInSeconds() {
		return validityInSeconds;
	}
	public void setValidityInSeconds(int validityInSeconds) {
		this.validityInSeconds = validityInSeconds;
	}
	public String getTimeZoneId() {
		return timeZoneId;
	}
	public void setTimeZoneId(String timeZoneId) {
		this.timeZoneId = timeZoneId;
	}
	public ZonedDateTime getExpiry() {
		return expiry;
	}
	public void setExpiry(ZonedDateTime expiry) {
		this.expiry = expiry;
	}
}
