package com.iwant.userservice.security.config;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

public class CustomTokenEnhancer  extends JwtAccessTokenConverter {
	
	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken defaultToken, OAuth2Authentication auth) {
		UsernamePasswordAuthenticationToken user = (UsernamePasswordAuthenticationToken) auth.getUserAuthentication(); //auth.getPrincipal();
		
		Map<String,Object> info = new LinkedHashMap(defaultToken.getAdditionalInformation());
		
		
		if(user.getPrincipal()!=null)
			info.put("userName", user.getPrincipal());
		
		if(user.getAuthorities()!=null)
			info.put("Granted-Authorities", user.getAuthorities());
		
		DefaultOAuth2AccessToken newToken = new DefaultOAuth2AccessToken(defaultToken);
		newToken.setAdditionalInformation(info);
		
		return super.enhance(newToken, auth);
		
	}
}
