package com.iwant.userservice.security.helper;

public class PasswordAttemptSession {
	private String userId;
	private int maxAttempts;
	private int passwordAttempts;
	
	public PasswordAttemptSession(String userId,int maxAttempts) {
		this.userId = userId;
		this.maxAttempts = maxAttempts;
	}
	public int newAttempt() {
		return ++this.passwordAttempts;
	}
	public boolean canAttempt() {
		return this.passwordAttempts < maxAttempts;
	}
}
