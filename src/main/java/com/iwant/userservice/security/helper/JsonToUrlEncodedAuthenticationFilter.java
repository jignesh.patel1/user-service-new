package com.iwant.userservice.security.helper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Objects;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.catalina.connector.RequestFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.iwant.userservice.UserServiceApplication;

import org.codehaus.jackson.map.ObjectMapper;

@Component
@Order(value = Integer.MIN_VALUE)
public class JsonToUrlEncodedAuthenticationFilter implements Filter {

	private static final Logger logger = LoggerFactory.getLogger(JsonToUrlEncodedAuthenticationFilter.class);
			
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    	logger.info("Filter Chain:"+filterConfig.getFilterName());
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {
    	
    	UserServiceApplication.filterIndex++;
    	int size = request.getContentLength();
    	
    	HttpServletRequest httpRequest = (HttpServletRequest)request;
    	String path = httpRequest.getRequestURI();
    	logger.info("Requested URI:"+path + "::" + request.getContentType() +", Size:"+size +", index:" + UserServiceApplication.filterIndex);
    	
        //if (Objects.equals(request.getContentType(), "application/json") && Objects.equals(((RequestFacade) request).getServletPath(), "/oauth/token")) {
    	if (Objects.equals(request.getContentType(), "application/json") && Objects.equals(path, "/oauth/token")) {
            InputStream is = request.getInputStream();
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            
            int nRead;
            byte[] data = new byte[16384];

            while ((nRead = is.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }
            buffer.flush();
            byte[] json = buffer.toByteArray();
            String jsonBody = new String(json);
            
            try {
	            HashMap result = new ObjectMapper().readValue(jsonBody, HashMap.class);
	            logger.trace(result.toString());	
	            HashMap<String, String[]> r = new HashMap<>();
	            for (Object key : result.keySet()) {
	                String[] val = new String[1];
	                val[0] = (String)result.get(key);
	                r.put(key.toString(), val);
	            }
	            logger.trace(r.toString());
	
	            String[] val = new String[1];
	            val[0] = ((RequestFacade) request).getMethod();
	            r.put("_method", val);
	
	            HttpServletRequest s = new CustomServletRequestMapper(((HttpServletRequest) request), r);
	            logger.info("CustomServlet Created for filterIndex:"+UserServiceApplication.filterIndex+", username:"+s.getParameter("username"));
	            chain.doFilter(s, response);
            }catch(Exception e) {
            	logger.info(e.getMessage());
            	logger.info("filterIndex@"+UserServiceApplication.filterIndex);
            	chain.doFilter(request, response);
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
    }

	
}
