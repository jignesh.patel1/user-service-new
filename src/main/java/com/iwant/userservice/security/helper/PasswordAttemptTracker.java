package com.iwant.userservice.security.helper;

import java.util.Hashtable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;



@Service
public class PasswordAttemptTracker {
	
	private static final Logger logger = LoggerFactory.getLogger(PasswordAttemptTracker.class);
	private Hashtable<String,PasswordAttemptSession> sessionManager = new Hashtable();
	
	public PasswordAttemptSession createSession(String user, PasswordAttemptSession session) {
		if(sessionManager.containsKey(user)) {
			return sessionManager.get(user);
		}else {
			sessionManager.put(user, session);
			return session;
		}
	}
	public boolean release(String user) {
		if(sessionManager.containsKey(user)) {
			sessionManager.remove(user);
			logger.trace("Password Attempt Removed for user:"+user);
			return true;
		}else {
			logger.trace("Password Attempt session not found for given userid:"+user +". Hence cannot be released");
			return false;
		}
	}
	
}