package com.iwant.userservice.utils;

import java.math.BigInteger;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class CodeGenerator {
	//Linear Congruential random number generator for generating a unique code given that seed value is unique.
	private static final Logger logger = LoggerFactory.getLogger(CodeGenerator.class);
	private static final BigInteger preA = new BigInteger("3781927463263421");
	private static final BigInteger preC = new BigInteger("2113323684682149");
	private int codeLength;
	
	public CodeGenerator() {
		this.codeLength = 12; // Default length
	}
	public CodeGenerator(int codeLength) {
		super();
		this.codeLength = codeLength;
	}
	public String generateAccessCode(long seedValue) {
		logger.info("Generating code for seed value:"+seedValue);
		
		long lMod = (long) Math.pow(10,this.codeLength); 
		
		long lC = (long) Math.round((0.5 - (Math.sqrt(3)/6)) * lMod);
		BigInteger preM = new BigInteger(String.valueOf(lMod));
		BigInteger x = new BigInteger(seedValue+"");
		BigInteger y = x.multiply(preA).add(preC);
		String res = y.mod(preM).toString();
		logger.info("Generated code:"+res);
		while(res.length() < codeLength) res = "0"+res; // supply leading 0s to small numbers.
		return res;
	}

}

