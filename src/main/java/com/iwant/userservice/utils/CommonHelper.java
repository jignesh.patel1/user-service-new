package com.iwant.userservice.utils;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class CommonHelper {
	private static final Logger logger = LoggerFactory.getLogger(CommonHelper.class);
	
	public static Date getCurrentTimestamp(String timezone){
		try {
			SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
			dateFormatGmt.setTimeZone(TimeZone.getTimeZone(timezone));
	
			//Local time zone   
			SimpleDateFormat dateFormatLocal = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
	
			//Time in GMT
			return dateFormatLocal.parse( dateFormatGmt.format(new Date()) );
		}catch(ParseException e) {
			
			e.printStackTrace();
		}
		return null;
	}
	public static List<String> getTimeZoneList(){
		String[] id = TimeZone.getAvailableIDs();
		List<String> items = new ArrayList();
		//System.out.println("In TimeZone class available Ids are: ");  
		for (int i=0; i<id.length; i++){  
			items.add(id[i]);
		}
		return items;
	   
	}
	public static String generateOtp(int length) {
		StringBuilder generatedToken = new StringBuilder();
        try {
            SecureRandom number = SecureRandom.getInstance("SHA1PRNG");
            // Generate 20 integers 0..20
            for (int i = 0; i < length; i++) {
                generatedToken.append(number.nextInt(9));
            }
            return generatedToken.toString();
        } catch (NoSuchAlgorithmException e) {
        	logger.error("Error while generating otp:"+e.getMessage());
            e.printStackTrace();
            return "-1";
        }
	}

}
