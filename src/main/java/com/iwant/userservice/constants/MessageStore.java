package com.iwant.userservice.constants;

public interface MessageStore {

	public static final String MSG_USER_NOT_FOUND = "security.login.usernotfound";
	public static final String MSG_BAD_REQUEST = "security.login.invalidrequest";
	public static final String MSG_LOGIN_FAILED = "security.login.failed";
	public static final String MSG_INVALID_OTP = "security.login.invalidotp";
	public static final String MSG_MAX_ATTEMPT_ERROR = "security.login.password.attempt.error";
	
	public static final String MSG_REG_MOBILE_REQUIRED = "user.registration.mobile.missing";
	public static final String MSG_REG_EMAIL_REQUIRED = "user.registration.email.missing";
	public static final String MSG_REG_EMAIL_OR_MOBILE_REQUIRED = "user.registration.emailOrMobile.missing";
	public static final String MSG_REG_USER_EXISTS = "user.registration.user.exists";
	
	
	
	
}
