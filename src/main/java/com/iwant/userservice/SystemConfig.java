package com.iwant.userservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.iwant.userservice.wrapper.LocaleOption;


@Configuration
public class SystemConfig {
	
	public final static int USER_SYS_ADMIN = -1;
	public final static int USER_CUSTOMER = 0;
	public final static int USER_STAFF = 1;
	public final static int USER_MERCHANT = 2;
	public final static int USER_PARTNER = 3;
	public final static int USER_OTHER = 4;
	
	@Value("${iwant.user-service.admin.username}")
	String username;
	
	@Value("${iwant.user-service.admin.password}")
	String password;
	
	@Value("${iwant.user-service.admin.mcc}")
	String mcc;
	
	@Value("${iwant.user-service.admin.mobile}")
	String mobile;
	
	@Value("${iwant.user-service.admin.email}")
	String email;
	
	@Value("${iwant.user-service.admin.firstname}")
	String firstName;
	
	@Value("${iwant.user-service.admin.lastname}")
	String lastName;
	
	@Value("${iwant.user-service.admin.domain}")
	String domain;
	
	@Value("${iwant.user-service.admin.locale}")
	String locale;
	
	@Value("${iwant.user-service.admin.languageName}")
	String languageName;
	
	
	@Value("${iwant.user-service.admin.offsetId}")
	String offsetId;
	
	@Value("${iwant.user-service.admin.country}")
	String country;
	
	
	@Bean
	public AdminUser getSystemAdminUser() {
		AdminUser adminUser = new AdminUser();
		adminUser.username = username;
		adminUser.password = password;
		adminUser.mcc = mcc;
		adminUser.mobilenumber = mobile;
		adminUser.email = email;
		adminUser.firstName = firstName;
		adminUser.lastName = lastName;
		
		return adminUser;
	}
	@Bean
	public DefaultValues getDefaultValues() {
		DefaultValues config = new DefaultValues();
		config.domain = domain;
		config.locale = locale;
		config.offsetId = offsetId;
		return config;
	}
	@Bean
	public LocaleOption getDefaultLocaleOption() {
		LocaleOption defaultLocale = new LocaleOption();
		defaultLocale.setLanguage(languageName);
		defaultLocale.setLocaleId(locale);
		defaultLocale.setMcc(mcc);
		defaultLocale.setCountry(country);
		return defaultLocale;
	}
	
	public class AdminUser{
		public String username;
		public String password;
		public String mcc;
		public String mobilenumber;
		public String email;
		public String firstName;
		public String lastName;
	} 
	public class DefaultValues{
		public String locale;
		public String offsetId;
		public String domain;
	}
	

}
