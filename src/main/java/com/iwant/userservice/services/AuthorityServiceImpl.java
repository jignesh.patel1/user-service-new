package com.iwant.userservice.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.iwant.userservice.data.models.security.AuthorityModel;
import com.iwant.userservice.services.interfaces.AuthorityService;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class AuthorityServiceImpl implements AuthorityService {

	@Autowired
	MongoTemplate mongoService;
	@Override
	public List<AuthorityModel> findAll(String domain) {
		Query query = new Query(Criteria.where("domain").is(domain));
		query.with(Sort.by("name").ascending());
		return mongoService.find(query, AuthorityModel.class);
	}
	@Override
	public List<AuthorityModel> findAllByActive(String domain) {
		Query query = new Query(Criteria.where("domain").is(domain).andOperator(Criteria.where("isActive").is(true)));
		query.with(Sort.by("name").ascending());
		return mongoService.find(query, AuthorityModel.class);
	}
	@Override
	public AuthorityModel findById(String id) {
		Query query = new Query(Criteria.where("id").is(id));
		return mongoService.findOne(query, AuthorityModel.class);
	}

	@Override
	public AuthorityModel create(AuthorityModel model) {
		
		return mongoService.insert(model);
	}

	@Override
	public AuthorityModel update(AuthorityModel model) {
		// TODO Auto-generated method stub
		return mongoService.save(model);
	}

	@Override
	public AuthorityModel delete(AuthorityModel model) {
		// TODO Auto-generated method stub
		return mongoService.findAndRemove(new Query(Criteria.where("_id").is(model.getId())), AuthorityModel.class);
	}
	@Override
	public List<AuthorityModel> findAllById(List<String> ids) {
		
		Query query = new Query(Criteria.where("_id").in(ids).andOperator().where("isActive").is(true));
		return mongoService.find(query, AuthorityModel.class);
	}
	@Override
	public long findRecordCount() {
		// TODO Auto-generated method stub
		return mongoService.count(new Query(), AuthorityModel.class);
	}

}
