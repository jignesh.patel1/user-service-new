package com.iwant.userservice.services;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.iwant.userservice.data.models.SequenceId;
import com.iwant.userservice.data.models.StateModel;
import com.iwant.userservice.services.common.CommonServices;
import com.iwant.userservice.services.interfaces.StateService;

@Service
public class StateServiceImpl implements StateService {

	private static final Logger logger = LoggerFactory.getLogger(StateServiceImpl.class);
	private static final String STATE_SEQ_KEY = "STATE";
	
	@Autowired
	MongoTemplate mongoService;
	
	@Autowired
	CommonServices commonService;
	
	@Override
	public StateModel findOneById(String id) {
		Query query = new Query(Criteria.where("id").is(id));
		return mongoService.findOne(query,StateModel.class);
	}

	@Override
	public StateModel findOneByCode(String code) {
		Query query = new Query(Criteria.where("code").is(code));
		return mongoService.findOne(query,StateModel.class);
		
	}

	@Override
	public List<StateModel> findAll(String domain) {
		Query query = new Query(Criteria.where("domain").is(domain));
		return mongoService.find(query,StateModel.class);
	}

	@Override
	public StateModel create(StateModel model) {
		logger.info("creating new State:");
		Query query = new BasicQuery("{}");
		long count = mongoService.count(query, StateModel.class);
		logger.info(count + " States exist as of now");
		if(count < 1) {
			SequenceId seq = commonService.createNewSequence(STATE_SEQ_KEY, model.getDomain());
			logger.info("Sequence ["+seq.getId()+"] Created Successfully");
		}
		model.setCode(commonService.getNextSequence(STATE_SEQ_KEY, model.getDomain()));
		model.setCreatedOn(new Date());
		
		return mongoService.insert(model);
		
	}

	@Override
	public StateModel update(StateModel model) {
		model.setUpdatedOn(new Date());
		return mongoService.save(model);
	}

	@Override
	public StateModel findAndRemove(String id) {
		Query query = new Query(Criteria.where("id").is(id));
		return mongoService.findAndRemove(query, StateModel.class);
	}
	@PreAuthorize("hasAuthority('SYSTEM_ADMIN') or hasAuthority('ROLE_VIEW_STATE')")
	@Override
	public List<StateModel> findAllByCountry(String id) {
		Query query = new Query(Criteria.where("country.id").is(id));
		return mongoService.find(query,StateModel.class);
	}

}
