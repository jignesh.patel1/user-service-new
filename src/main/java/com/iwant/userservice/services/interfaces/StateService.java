package com.iwant.userservice.services.interfaces;

import java.util.List;

import com.iwant.userservice.data.models.StateModel;

public interface StateService {
	
	public StateModel findOneById(String id);
	public StateModel findOneByCode(String code);
	public List<StateModel> findAll(String domain);
	public StateModel create(StateModel model);
	public StateModel update(StateModel model);
	public StateModel findAndRemove(String id);
	public List<StateModel> findAllByCountry(String id);

}
