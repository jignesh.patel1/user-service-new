package com.iwant.userservice.services.interfaces;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.iwant.userservice.data.models.security.AccessGroupModel;

public interface AccessGroupService {
	
	public List<AccessGroupModel> findAll(String domain);
	public AccessGroupModel create(AccessGroupModel req);
	public AccessGroupModel update(AccessGroupModel req);
	public AccessGroupModel findById(String id);
	public AccessGroupModel findSystemAdmin() ;
	public AccessGroupModel findForCustomer() ;
	
	
	
}
