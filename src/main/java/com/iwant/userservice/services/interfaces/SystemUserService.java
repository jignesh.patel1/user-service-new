package com.iwant.userservice.services.interfaces;

import java.util.List;

import com.iwant.userservice.data.models.security.SystemUser;
import com.iwant.userservice.wrapper.LocaleOption;

public interface SystemUserService {
	
	public SystemUser create(SystemUser user);
	
	public SystemUser findById(String id);
	public SystemUser findByEmail(String mobile, String domain, int userType);
	public SystemUser findByMobile(String mobile, String domain, int userType);
	public SystemUser findByIdentity(String identity,String domain, int userType);
	public SystemUser setForcePassword(SystemUser use);
	public SystemUser resetPassword(String id,String transactionId,String otp, String newPassword,String changeInfo);
	public SystemUser setLocale(String id,LocaleOption locale,String changeInfo);
	public SystemUser assignAccessGroup(String id,List<String> ids);
	public List<SystemUser> findAll();
	public long getUserCount();
}
