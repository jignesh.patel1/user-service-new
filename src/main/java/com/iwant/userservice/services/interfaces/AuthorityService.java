package com.iwant.userservice.services.interfaces;

import java.util.List;

import com.iwant.userservice.data.models.security.AuthorityModel;

public interface AuthorityService {

	public long findRecordCount();
	public List<AuthorityModel> findAll(String domain);
	public List<AuthorityModel> findAllByActive(String domain);
	public List<AuthorityModel> findAllById(List<String> ids);
	public AuthorityModel findById(String id);
	public AuthorityModel create(AuthorityModel model);
	public AuthorityModel update(AuthorityModel model);
	public AuthorityModel delete(AuthorityModel model);
}
