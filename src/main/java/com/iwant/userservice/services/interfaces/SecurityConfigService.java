package com.iwant.userservice.services.interfaces;

import com.iwant.userservice.data.models.security.SecurityConfig;

public interface SecurityConfigService {
	
	public SecurityConfig findByDomain(String domain);
	public SecurityConfig create(SecurityConfig model);
	public SecurityConfig update(SecurityConfig model);
	public SecurityConfig.TokenConfig findTokenConfigFor(String device);
}
