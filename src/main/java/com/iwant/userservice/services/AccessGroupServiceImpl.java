package com.iwant.userservice.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.iwant.userservice.data.models.security.AccessGroupModel;
import com.iwant.userservice.data.models.security.AuthorityModel;
import com.iwant.userservice.services.interfaces.AccessGroupService;
import com.iwant.userservice.services.interfaces.AuthorityService;

@Service
public class AccessGroupServiceImpl implements AccessGroupService {

	private static final Logger logger = LoggerFactory.getLogger(AccessGroupServiceImpl.class);
	@Autowired
	MongoTemplate mongoService;
	
	@Autowired
	AuthorityService authorityService;
	
	@Override
	public List<AccessGroupModel> findAll(String domain) {
		Query query = new Query(Criteria.where("domain").is(domain));
		return mongoService.find(query, AccessGroupModel.class);
	}

	@Override
	public AccessGroupModel create(AccessGroupModel req) {
		List<String> ids = req.getAuthorities();
		if(ids.size()>0) {
			List<AuthorityModel> permissions = authorityService.findAllById(ids);
			logger.info(permissions.size() + " :Permissions found for provided authority ids");
			if(permissions.size()>0) {
				req.setPermissions(permissions);
			}
		}else {
			logger.info("Authorities not provided");
		}
		return mongoService.insert(req);
	}

	@Override
	public AccessGroupModel update(AccessGroupModel req) {
		// TODO Auto-generated method stub
		
		return mongoService.save(req);
	}

	@Override
	public AccessGroupModel findById(String id) {
		// TODO Auto-generated method stub
		return mongoService.findOne(new Query(Criteria.where("_id").is(id)), AccessGroupModel.class);
	}
	
	@Override
	public AccessGroupModel findSystemAdmin() {
		// TODO Auto-generated method stub
		Query query = new Query(Criteria.where("name").is("SYSTEM_ADMINISTRATOR").andOperator(Criteria.where("isSystemGenerated").is(true)));
		return mongoService.findOne(query, AccessGroupModel.class);
	}
	@Override
	public AccessGroupModel findForCustomer() {
		Query query = new Query(Criteria.where("name").is("DEFAULT_CUSTOMER_GROUP").andOperator(Criteria.where("isSystemGenerated").is(true)));
		return mongoService.findOne(query, AccessGroupModel.class);
	}

	

}
