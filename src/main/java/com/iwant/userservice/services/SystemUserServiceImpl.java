package com.iwant.userservice.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.iwant.userservice.SystemConfig;
import com.iwant.userservice.UserServiceApplication;
import com.iwant.userservice.constants.MessageStore;
import com.iwant.userservice.data.models.security.AccessGroupModel;
import com.iwant.userservice.data.models.security.AuthorityModel;
import com.iwant.userservice.data.models.security.SecurityConfig;
import com.iwant.userservice.data.models.security.SystemUser;
import com.iwant.userservice.exceptions.UserOperationException;
import com.iwant.userservice.exceptions.UserRegistrationException;
import com.iwant.userservice.services.interfaces.AccessGroupService;
import com.iwant.userservice.services.interfaces.AuthorityService;
import com.iwant.userservice.services.interfaces.OtpService;
import com.iwant.userservice.services.interfaces.SystemUserService;
import com.iwant.userservice.utils.CommonHelper;
import com.iwant.userservice.wrapper.LocaleOption;
import com.iwant.userservice.wrapper.OtpValidation;

@Service
public class SystemUserServiceImpl implements SystemUserService,MessageStore {

	private static final Logger logger = LoggerFactory.getLogger(SystemUserServiceImpl.class);
			
	@Autowired
	MongoTemplate mongoService;
	
	@Autowired
	PasswordEncoder encoder;
	
	@Autowired
	MessageSource messageSource;
	
	@Autowired
	AccessGroupService accessGroupService;
	
	@Autowired
	AuthorityService authorityService;
	
	@Autowired
	OtpService otpService;
	
	private String build(String code, Locale locale) {
		return messageSource.getMessage(code, null, locale);
	}
	@Override
	public SystemUser create(SystemUser user) throws UserRegistrationException {
		
		SecurityConfig config = UserServiceApplication.configContext.get(user.getDomain());
		
		LocaleOption defaultLocaleOption = config.getDefaultLocale();
		Locale defaultLocale = defaultLocaleOption.getInstance();
		boolean mobilePresent = user.getMobile()!=null && user.getMobile().length()>7;
		boolean emailPresent = user.getEmail() !=null && user.getEmail().length() > 7;
		
		if(config.isMobileRequired() && !mobilePresent) {
			throw new UserRegistrationException(build(this.MSG_REG_MOBILE_REQUIRED,defaultLocale));
		}
		if(config.isEmailRequired() && !emailPresent) {
			throw new UserRegistrationException(build(this.MSG_REG_EMAIL_REQUIRED,defaultLocale));
		}
		if(config.isEitherMobileOrEmailRequired() && (!emailPresent || !mobilePresent)) {
			throw new UserRegistrationException(build(this.MSG_REG_EMAIL_OR_MOBILE_REQUIRED,defaultLocale));
		}
		
		String username = user.getUsername()!=null? user.getUsername():(mobilePresent? user.getMobile() : user.getEmail());
		
		logger.info(config.getDomain()+":"+config.getPasswordValidityInDays());
		//SystemUser mobile = findByMobile(user.getMobile(),user.getDomain(),user.getUserType());
		//SystemUser email = findByEmail(user.getEmail(),user.getDomain(),user.getUserType());
		boolean userExists = userExist(username, user.getMobile(),user.getEmail(),user.getDomain(),user.getUserType());
		if(userExists) {
			logger.info("User Already exists with given mobile number or email");
			throw new UserRegistrationException(build(MSG_REG_USER_EXISTS, defaultLocale));
		}
		
		if(user.getMcc()==null || user.getMcc().length()<1 || !user.getMcc().startsWith("+")) {
			user.setMcc(defaultLocaleOption.getMcc());
		}
		
		user.setUsername(username);
		user.setLocale(defaultLocaleOption);
		user.setOffsetId(config.getOffsetId());
		user.setRemark("Registration");
		
		Calendar expiry = Calendar.getInstance();
		user.setCreatedOn(new Date(expiry.getTimeInMillis()));
		expiry.add(Calendar.DATE, config.getPasswordValidityInDays());

		if(user.getPassword()==null || user.getPassword().length()<1) {
			user.setPassword(CommonHelper.generateOtp(6));
			user.setForcePasswordChange(true);
		}
		user.setPassword(encrypt(user.getPassword(),user.getEncryptionType()));
		user.setPasswordValidityDays(config.getPasswordValidityInDays());
		user.setPasswordExpiry(new Date(expiry.getTimeInMillis()));
		switch(user.getUserType()) {
		case SystemConfig.USER_CUSTOMER:
		case SystemConfig.USER_SYS_ADMIN:
			user.setAuthorities(getDefaultAuthorities(user.getUserType()));
			break;
		}
		
		return mongoService.insert(user);
	}
	
	@Override
	public SystemUser findById(String id) {
		Query query = new Query(Criteria.where("_id").is(id));
		return mongoService.findOne(query, SystemUser.class);
	}

	
	@Override
	public SystemUser findByMobile(String mobile, String domain, int userType) {
		Query query = new Query(Criteria.where("mobile").is(mobile).and("domain").is(domain).and("userType").is(userType));
		
		return mongoService.findOne(query, SystemUser.class);
	}
	
	@Override
	public SystemUser findByEmail(String email, String domain, int userType) {
		Query query = new Query(Criteria.where("email").is(email).and("domain").is(domain).and("userType").is(userType));
		return mongoService.findOne(query, SystemUser.class);
	}

	@Override
	public List<SystemUser> findAll() {
		
		return mongoService.findAll(SystemUser.class);
	}

	@Override
	public long getUserCount() {
		
		return mongoService.count(new BasicQuery("{}") , SystemUser.class);
	}
	private boolean userExist(String username, String mobile, String email, String domain, int accountType) {
		String queryString = String.format(
				"{ "
				+ "domain : \"%s\","
				+ "userType : %d ,"
				+ "$or : "
						+ "["
						+ "{username : \"%s\"},"
						+ "{mobile : \"%s\"},"
						+ "{email : \"%s\"},"
						+ "], "
				
				+ "}",
				domain,accountType,username, mobile,email);
		logger.info(queryString);
		long count = mongoService.count(new BasicQuery(queryString), SystemUser.class);
		logger.info("Total %d user found with given information(%s,%s,%s) in domain %s with account type: %d",count,username,mobile,email,domain,accountType);
		return count>0?true:false;
	}
	@Override
	public SystemUser findByIdentity(String identity, String domain, int userType) {
		String queryString = String.format(
				"{ "
				+ "domain : \"%s\","
				+ "userType : %d ,"
				+ "$or : "
						+ "["
						+ "{username : \"%s\"},"
						+ "{mobile : \"%s\"},"
						+ "{email : \"%s\"},"
						+ "{externalReferenceId : \"%s\"}"
						+ "], "
				
				+ "}",
				domain,userType,identity, identity,identity, identity);
		
		logger.info(queryString);
		Query query = new BasicQuery(queryString);
		//Query query = new Query(Criteria.where("username").is(identity).and("domain").is(domain).and("userType").is(userType));
		SystemUser selected = mongoService.findOne(query, SystemUser.class);
		if(selected != null) {
			return selected;
		}else {
			logger.info("User not found :" + identity);
			return null;
		}
	}
	private String encrypt(String data,int mode) {
		switch(mode) {
		case 0:
			return data;
		case 1:
			return encoder.encode(data);
		}
		return data;
	}
	public List<String> getDefaultAuthorities(int accountType){
		ArrayList<String> authorities = new ArrayList();
		switch(accountType) {
		case SystemConfig.USER_CUSTOMER:
			AccessGroupModel customerAccessGroup = accessGroupService.findForCustomer();
			List<AuthorityModel> permissions = customerAccessGroup.getPermissions();
			for(AuthorityModel permission: permissions) {
				authorities.add(permission.getRole());
			}
			break;
		case SystemConfig.USER_SYS_ADMIN:
			AccessGroupModel adminAccessGroup = accessGroupService.findSystemAdmin();
			List<AuthorityModel> adminPermissions = adminAccessGroup.getPermissions();
			for(AuthorityModel permission: adminPermissions) {
				authorities.add(permission.getRole());
			}
			break;
		}
		return authorities;
	}
	@Override
	public SystemUser setForcePassword(SystemUser user) {
		user.setForcePasswordChange(true);
		return mongoService.save(user);
	}
	@Override
	public SystemUser resetPassword(String id, String transactionId, String otp, String newPassword, String changeInfo) throws UserOperationException{
		SystemUser user = findById(id);
		if(user != null) {
			OtpValidation otpValidation = otpService.validateOtp(transactionId, otp, user.getUsername(), user.getDomain());
			if(otpValidation.isValid()) {
				user.setPassword(encrypt(newPassword,user.getEncryptionType()));
				return mongoService.save(user);
			}else {
				throw new UserOperationException(messageSource.getMessage(this.MSG_INVALID_OTP, null, user.getLocale().getInstance()));
			}
		}else {
			String message = String.format(messageSource.getMessage(this.MSG_USER_NOT_FOUND, null, user.getLocale().getInstance()),user.getUsername());
			throw new UserOperationException(message);
		}
		
	}
	@Override
	public SystemUser setLocale(String id, LocaleOption locale, String changeInfo) {
		SystemUser user = mongoService.findOne(new Query(Criteria.where("_id").is(id)), SystemUser.class);
		if(user != null) {
			user.setLocale(locale);
			user.setChangeInfo(changeInfo);
			return mongoService.save(user);
		}else {
			logger.error("User not found");
			throw new UserOperationException("User not found");
		}
		
	}
	@Override
	public SystemUser assignAccessGroup(String id, List<String> ids)throws UserOperationException {
		
		SystemUser user = mongoService.findOne(new Query(Criteria.where("_id").is(id)), SystemUser.class);
		if(user != null) {
			List<AuthorityModel> authorities = authorityService.findAllById(ids);
			if(authorities.size()==0 && authorities.size() == ids.size()) {
				List<String> permissions = new ArrayList();
				for(AuthorityModel authority: authorities) {
					permissions.add(authority.getRole());
				}
				user.setAuthorities(permissions);
				return mongoService.save(user);
			}else {
				logger.error("Permissions not found for given authority list");
				throw new UserOperationException("Permissions not found for given authority list");
			}
		}else {
			logger.error("User not found");
			throw new UserOperationException("User not found");
		}
		
	}
	

}
