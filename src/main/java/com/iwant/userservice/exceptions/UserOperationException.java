package com.iwant.userservice.exceptions;

public class UserOperationException extends RuntimeException {
	public UserOperationException(String message) {
		super(message);
	}

}
