package com.iwant.userservice.exceptions;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

public class UserRegistrationException extends RuntimeException{

	
	
	public UserRegistrationException(String message) {
		super(message);
		
	}
}
