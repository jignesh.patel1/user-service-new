package com.iwant.userservice;

import com.iwant.userservice.data.models.security.AccessGroupModel;
import com.iwant.userservice.data.models.security.AuthorityModel;
import com.iwant.userservice.data.models.security.SecurityConfig;
import com.iwant.userservice.data.models.security.SystemUser;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.Banner;
import org.springframework.boot.Banner.Mode;
import org.springframework.boot.ResourceBanner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;

import com.iwant.userservice.data.models.CountryModel;
import com.iwant.userservice.services.interfaces.AccessGroupService;
import com.iwant.userservice.services.interfaces.AuthorityService;
import com.iwant.userservice.services.interfaces.CountryService;
import com.iwant.userservice.services.interfaces.SecurityConfigService;
import com.iwant.userservice.services.interfaces.SystemUserService;
import com.iwant.userservice.utils.CommonHelper;
import com.iwant.userservice.wrapper.LocaleOption;

import org.codehaus.jackson.map.ObjectMapper;

@SpringBootApplication
public class UserServiceApplication { //implements CommandLineRunner {
	
	private static final Logger logger = LoggerFactory.getLogger(UserServiceApplication.class);

	public static int filterIndex;
	@Autowired
	private static Environment environment;
	public static Hashtable<String,SecurityConfig> configContext = new Hashtable();
	public static ApplicationContext context;
	
	public static String DEFAULT_ADMIN_AUTHORITY;
	public static String DEFAULT_CUSTOMER_AUTHORITY;
	
	
	
	public static void main(String[] args) {
		
		SpringApplication app = new SpringApplication(UserServiceApplication.class); 
        
		app.setBannerMode(Mode.CONSOLE);
        context = app.run(args);
        
        SystemConfig appConfig = context.getBean(SystemConfig.class);
        SystemConfig.AdminUser systemAdmin = appConfig.getSystemAdminUser();
        SystemConfig.DefaultValues defaultValues = appConfig.getDefaultValues();
        LocaleOption defaultLocaleOption = appConfig.getDefaultLocaleOption();
        
		/***
		 * Step 1: Default security configuration
		 * */
        SecurityConfigService configManager = context.getBean(SecurityConfigService.class);
		SecurityConfig defaultConfig = configManager.findByDomain("Default");
		if(defaultConfig == null) {
			defaultConfig = new SecurityConfig();
			defaultConfig.setDomain(defaultValues.domain);
			defaultConfig.setMaxAttampts(3);
			defaultConfig.setPasswordValidityInDays(365);
			SecurityConfig.TokenConfig tConfig = defaultConfig.tokenConfig("Any", 180,60*60*24*180);
			defaultConfig.setDefaultTokenConfig(tConfig);
			defaultConfig.setOtpLength(4);
			defaultConfig.setOtpValidityinSeconds(60);
			SecurityConfig.OtpDeliveryMode sms = defaultConfig.otpDeliveryMode(1, "SMS");
			SecurityConfig.OtpDeliveryMode email = defaultConfig.otpDeliveryMode(2, "EMAIL");
			Collection<SecurityConfig.OtpDeliveryMode> otpDeliveryConfig = new Vector();
			otpDeliveryConfig.add(sms);
			otpDeliveryConfig.add(email);
			defaultConfig.setOtpDeliveryConfig(otpDeliveryConfig);
			
			defaultConfig.setOffsetId(defaultLocaleOption.getLocaleId());
			
			defaultConfig.setDefaultLocale(defaultLocaleOption);
			defaultConfig = configManager.create(defaultConfig);
			
			logger.info("Added default security Config successfully:"+defaultConfig.getId());
		}else {
			logger.info("Default Security Config already exists");
		}
		configContext.put("Default", defaultConfig);
		/**
		 * Steo 2: create Default Authorities & Access groups
		 * */
		String [] ids = createDefaultAuthorities();
		
		/***
		 * Step 3: Create Super Admin User
		 * */
		
		SystemUserService userService = context.getBean(SystemUserService.class);
		AuthorityService authorityService = context.getBean(AuthorityService.class);
		AccessGroupService accessGroupService = context.getBean(AccessGroupService.class);
		
		long totalUsers = userService.getUserCount();
		logger.info(totalUsers + " Users found");
		if(totalUsers<=0) {
			SystemUser adminUser = new SystemUser();
			adminUser.setDomain(defaultValues.domain);
			adminUser.setUserType(SystemConfig.USER_SYS_ADMIN);
			adminUser.setFirstName(systemAdmin.firstName);
			adminUser.setLastName(systemAdmin.lastName);
			adminUser.setUsername(systemAdmin.username);
			
			adminUser.setPassword(systemAdmin.password);
			adminUser.setEncryptionType(0);
			adminUser.setMobile(systemAdmin.mobilenumber);
			adminUser.setEmail(systemAdmin.email);
			adminUser.setAuditInfo("Created on Bootstrap");
			
			adminUser.setLocale(LocaleOption.getDefault());
			adminUser.setCreatedBy("System Bootstrap");
			adminUser = userService.create(adminUser);
			
			logger.info(adminUser.getId()+": Admin User Created Successfully");
		}
	
	}
	private static String[] createDefaultAuthorities() {
		String[] result = new String[2];
		AuthorityService authorityService = context.getBean(AuthorityService.class);
		AccessGroupService accessGroupService = context.getBean(AccessGroupService.class);
		long records = authorityService.findRecordCount();
		if(records<=0) {
			AuthorityModel superAdmin = new AuthorityModel();
			superAdmin.setRole("ROLE_SYSTEM_ADMIN");
			superAdmin.setSystemGenerated(true);
			superAdmin.setActive(true);
			superAdmin = authorityService.create(superAdmin);
			if(superAdmin.getId()!=null) {
				AccessGroupModel adminGroup = new AccessGroupModel();
				adminGroup.setName("SYSTEM_ADMINISTRATOR");
				List<String> ids = new ArrayList();
				ids.add(superAdmin.getId());
				adminGroup.setAuthorities(ids);
				adminGroup.setActive(true);
				adminGroup.setSystemGenerated(true);
				adminGroup = accessGroupService.create(adminGroup);
				if(adminGroup.getId()==null) {
					logger.error("Failed while creating Default System Administrator Access Group");
				}else
				{
					DEFAULT_ADMIN_AUTHORITY= adminGroup.getId();
					result[0] = adminGroup.getId();
					logger.info("System Admin AccessGroup Created Successfully:"+result[0]);
				}
			}else {
				logger.error("Failed while creating Default System Administrator Permissions");
			}
			AuthorityModel customerPermission = new AuthorityModel();
			customerPermission.setRole("ROLE_CUSTOMER");
			customerPermission.setSystemGenerated(true);
			customerPermission = authorityService.create(customerPermission);
			if(customerPermission.getId()!=null) {
				AccessGroupModel customerAccessGroup = new AccessGroupModel();
				customerAccessGroup.setName("DEFAULT_CUSTOMER_GROUP");
				List<String> ids = new ArrayList();
				ids.add(superAdmin.getId());
				customerAccessGroup.setAuthorities(ids);
				customerAccessGroup.setActive(true);
				customerAccessGroup.setSystemGenerated(true);
				customerAccessGroup = accessGroupService.create(customerAccessGroup);
				if(customerAccessGroup.getId()==null) {
					logger.error("Failed while creating Default Customer Access Group");
				}else {
					DEFAULT_CUSTOMER_AUTHORITY = customerAccessGroup.getId();
					result[1] = customerAccessGroup.getId();
					logger.info("Customer AccessGroup Created Successfully:"+result[1]);
				}
			}else {
				logger.error("Failed while creating Default Customer Access Permissions");
			}
		}else {
			logger.info("Default AccessGroup & Permission already exists!!");
		}
		return result;
	}
		
}
