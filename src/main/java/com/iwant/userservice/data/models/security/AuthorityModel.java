package com.iwant.userservice.data.models.security;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.iwant.userservice.data.models.BaseEntityModel;

@Document("authorities")
public class AuthorityModel extends BaseEntityModel{

	@Id
	private String id;
	@Indexed
	private String role;
	
	private boolean isSystemGenerated;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public boolean isSystemGenerated() {
		return isSystemGenerated;
	}
	public void setSystemGenerated(boolean isSystemGenerated) {
		this.isSystemGenerated = isSystemGenerated;
	}
}
