package com.iwant.userservice.data.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@ToString(includeFieldNames=true)
@Document (collection = "countries")
public class CountryModel extends BaseEntityModel {
	
	@Id
	@Getter private String id;
	
	@Indexed
	private long code;
	
	@Indexed
	private String name;
	
	
	private String timezone;
	
	private String mcc;
	
	private boolean isDefault;

	public String getId() {
		return id;
	}


	public long getCode() {
		return code;
	}

	public void setCode(long code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getMcc() {
		return mcc;
	}

	public void setMcc(String mcc) {
		this.mcc = mcc;
	}

	public boolean isDefault() {
		return isDefault;
	}

	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}
	
	
	
	
}
