package com.iwant.userservice.data.models;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("states")
public class StateModel extends BaseEntityModel{

	@Id
	private String id;
	
	@Indexed
	private long code;
	
	@Indexed
	private String name;
	
	
	
	
	@DBRef @Indexed
	private CountryModel country;
	
	private String timeZone;

	public String getId() {
		return id;
	}

	public long getCode() {
		return code;
	}

	public void setCode(long code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public CountryModel getCountry() {
		return country;
	}

	public void setCountry(CountryModel country) {
		this.country = country;
	}

		
}
