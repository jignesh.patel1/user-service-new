package com.iwant.userservice.data.models.security;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.iwant.userservice.data.models.BaseEntityModel;

@Document("accessgroup")
public class AccessGroupModel extends BaseEntityModel{

	@Id
	private String id;
	@Indexed
	private String name;
	private boolean isSystemGenerated;
	private List<String> authorities;
	private List<AuthorityModel> permissions;
	
	public String getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getAuthorities() {
		return authorities;
	}
	public void setAuthorities(List<String> authorities) {
		this.authorities = authorities;
	}

	public List<AuthorityModel> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<AuthorityModel> permissions) {
		this.permissions = permissions;
	}

	public boolean isSystemGenerated() {
		return isSystemGenerated;
	}

	public void setSystemGenerated(boolean isSystemGenerated) {
		this.isSystemGenerated = isSystemGenerated;
	}
	
	
}
