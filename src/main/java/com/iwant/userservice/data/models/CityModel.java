package com.iwant.userservice.data.models;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document
public class CityModel extends BaseEntityModel {
	
	@Id
	private String id;
	
	@Field("name")
	@Indexed
	private String name;
	
	private List<AreaModel> areas;
	
	private String timeZone;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<AreaModel> getAreas() {
		return areas;
	}

	public void setAreas(List<AreaModel> areas) {
		this.areas = areas;
	}

	public String getTimeZoneId() {
		return timeZone;
	}

	public void setTimeZoneId(String timeZoneId) {
		this.timeZone = timeZoneId;
	}
}
