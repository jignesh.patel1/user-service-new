package com.iwant.userservice.data.models.security;

import java.time.ZonedDateTime;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("otpinfo")
public class OtpModel {

	@Transient 
	public static final String OTP_REGISTRATION = "REG";
	@Transient 
	public static final String OTP_LOGIN = "LOG";
	@Transient
	public static final String OTP_PASSWORD_CHANGE = "PWD";
	@Transient
	public static final String OTP_ORDER ="ORD";
	@Transient
	public static final String OTP_PAYMENT = "PMT";
	
	@Id
	private String id;
	@Indexed
	private String domain;
	private String transactionCode;
	private String transactionType;
	@Indexed
	private String username;
	private String mcc;
	@Indexed
	private String mobile;
	@Indexed
	private String email;
	@Transient
	private int otpCode;
	private String otp;
	private int encryptionType;
	private int validityInSeconds;
	private String timeZone;
	private Date expiry;
	private Date createdOn;
	
	public String getId() {
		return id;
	}
	
	public String getTransactionCode() {
		return transactionCode;
	}
	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getMcc() {
		return mcc;
	}
	public void setMcc(String mcc) {
		this.mcc = mcc;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public int getEncryptionType() {
		return encryptionType;
	}
	public void setEncryptionType(int encryptionType) {
		this.encryptionType = encryptionType;
	}
	public int getValidityInSeconds() {
		return validityInSeconds;
	}
	public void setValidityInSeconds(int validityInSeconds) {
		this.validityInSeconds = validityInSeconds;
	}
	public Date getExpiry() {
		return this.expiry;
	}
	public void setExpiry(Date expiry) {
		this.expiry = expiry;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public int getOtpCode() {
		return otpCode;
	}

	public void setOtpCode(int otpCode) {
		this.otpCode = otpCode;
	}
}
