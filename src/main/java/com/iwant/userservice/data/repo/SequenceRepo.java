package com.iwant.userservice.data.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.iwant.userservice.data.models.SequenceId;



public interface SequenceRepo extends MongoRepository<SequenceId,String>{
}
