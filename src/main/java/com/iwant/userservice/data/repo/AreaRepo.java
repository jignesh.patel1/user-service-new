package com.iwant.userservice.data.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.iwant.userservice.data.models.AreaModel;

public interface AreaRepo extends MongoRepository<AreaModel,String>{

}
