package com.iwant.userservice.data.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.iwant.userservice.data.models.CountryModel;

public interface CountryRepo extends MongoRepository<CountryModel,String>{
	
	@Query ("{ domain : ?0")
	public List<CountryModel> findAllByDomain(String domain);
	

}
