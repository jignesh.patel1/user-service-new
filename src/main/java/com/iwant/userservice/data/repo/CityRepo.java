package com.iwant.userservice.data.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.iwant.userservice.data.models.CityModel;

public interface CityRepo extends MongoRepository<CityModel,String> {

}
